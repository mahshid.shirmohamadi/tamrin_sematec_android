package tamrin.sematec;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by masheloo on 12/30/2017.
 */

public class Fragment extends android.support.v4.app.Fragment {

public static Fragment frag;
public static Fragment newIntance(){
    if(frag==null)
        frag=new Fragment();
        return frag;


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_a_layout, container, false);

    return v;
    }
}



