package tamrin.sematec;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class StudentListActivity extends BaseActivity {
    ListView studentlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        studentlist = (ListView) findViewById(R.id.studentlist);

        StudentsModels student1 = new StudentsModels();
        student1.setStudentname("مهشید شیرمحمدی");
        student1.setFathername("محسن");
        student1.setStudentid(8656311);
        student1.setImagurl("https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Selena_Gomez_2008.jpg/220px-Selena_Gomez_2008.jpg");

        StudentsModels student2 = new StudentsModels();
        student2.setStudentname("مهدی اسکندری");
        student2.setFathername("حسن");
        student2.setStudentid(8750001);
        student2.setImagurl("https://i.pinimg.com/736x/c9/0d/1d/c90d1d4a838d8a2c6f47ebb38e1b4444--bradley-cooper-hair-mens-haircuts.jpg");


        final List<StudentsModels> student = new ArrayList<>();
        student.add(student1);
        student.add(student2);


        StudentListAdapter newadapter = new StudentListAdapter(mContext, student);

        studentlist.setAdapter(newadapter);

        studentlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    Intent intent = new Intent(mContext, ForResultActivityB.class);
                    startActivityForResult(intent, 205);


                }
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 205) {
            String result = data.getStringExtra("name");
            Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
        }


    }
}