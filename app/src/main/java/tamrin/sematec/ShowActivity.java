package tamrin.sematec;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        result=(TextView) findViewById(R.id.result);
        String ss = getIntent().getStringExtra("showname");
        result.setText(ss);
    }
}
