package tamrin.sematec.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tamrin.sematec.PublicMethods;
import tamrin.sematec.R;
import tamrin.sematec.models.Forecast;

import static tamrin.sematec.PublicMethods.weatherCheckType;

/**
 * Created by masheloo on 12/29/2017.
 */

public class ForcastAdapter extends BaseAdapter {

    Context mcontext;
    List<Forecast> castlist;


    public ForcastAdapter(Context mcontext, List<Forecast> castlist) {
        this.mcontext = mcontext;
        this.castlist = castlist;

    }


    @Override
    public int getCount() {
        return castlist.size();
    }

    @Override
    public Object getItem(int position) {
        return castlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View newview = LayoutInflater.from(mcontext).inflate(R.layout.forcast_list_item, parent, false);
        TextView date = newview.findViewById(R.id.date);
        TextView hightemp = newview.findViewById(R.id.hightemp);
        TextView lowtemp = newview.findViewById(R.id.lowtemp);
        TextView day = newview.findViewById(R.id.day);
        ImageView weatherimg = newview.findViewById(R.id.weatherimg);

        double hightempC = PublicMethods.converFtoC(Integer.parseInt(castlist.get(position).getHigh()));
        double lowtempC = PublicMethods.converFtoC(Integer.parseInt(castlist.get(position).getLow()));

        date.setText(castlist.get(position).getDate());
        hightemp.setText(String.format("High:%n %s°C", hightempC));
        lowtemp.setText(String.format("Low: %n %s°C ", lowtempC));
        day.setText(castlist.get(position).getDay());
        String yahootext = castlist.get(position).getText();


        weatherCheckType(yahootext, weatherimg);


        return newview;
    }


}
