package tamrin.sematec;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by masheloo on 12/20/2017.
 */

public class StudentListAdapter extends BaseAdapter {

    Context mcontext;
            List<StudentsModels> student;


    public StudentListAdapter(Context mcontext, List<StudentsModels> student) {
        this.mcontext = mcontext;
        this.student = student;
    }

    @Override
    public int getCount() {
        return student.size();
    }

    @Override
    public Object getItem(int position) {
        return student.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View newview = LayoutInflater.from(mcontext).inflate(R.layout.student_list_item, viewGroup, false);

        TextView name= newview.findViewById(R.id.studentname);
        ImageView image = newview.findViewById(R.id.img);
      //  TextView father = newview.findViewById(R.id.father);
        TextView studentfather = newview.findViewById(R.id.studentfather);
        TextView studentid = newview.findViewById(R.id.studentid );


        name.setText(student.get(position).getStudentname());
        studentfather.setText(student.get(position).getFathername());
        studentid.setText(student.get(position).getStudentid() + "");
        Picasso.with(mcontext).load(student.get(position).getImagurl()).into(image);






        return newview;
    }
}
