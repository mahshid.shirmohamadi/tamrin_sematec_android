package tamrin.sematec;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.ImageView;

import java.util.Objects;

/**
 * Created by masheloo on 1/1/2018.
 */

public class PublicMethods {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void weatherCheckType(String name, ImageView img) {

        if (Objects.equals("Sunny", name) || Objects.equals("Mostly Sunny", name)) {
            img.setImageResource(R.drawable.sunny);
        } else if (Objects.equals("Showers", name) || Objects.equals("Rain", name)) {

            img.setImageResource(R.drawable.showers);

        } else if (name.equals("Cloudy")) {

            img.setImageResource(R.drawable.cloudydaynight);

        } else if (name.equals("Mostly Cloudy")) {

            img.setImageResource(R.drawable.mostlycloudydaynight);

        } else if (name.equals("Partly Cloudy")) {

            img.setImageResource(R.drawable.partlycloudyday);

        } else if (name.equals("Fair")) {

            img.setImageResource(R.drawable.fairday);

        } else if (name.equals("Breezy")) {

            img.setImageResource(R.drawable.windydaynight);

        } else if (name.equals("Clear")) {

            img.setImageResource(R.drawable.clearnight);

        } else if (name.equals("Scattered Thunderstorms")) {

            img.setImageResource(R.drawable.scatteredshowersdaynight);

        } else
            img.setImageResource(R.drawable.ic_launcher_foreground);


    }



    public static float converFtoC (int f) {
    double c = (f - 32) / 1.8 ;
    return Math.round(c * 100)/100;

    }






}
