package tamrin.sematec;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import tamrin.sematec.adapters.ForcastAdapter;
import tamrin.sematec.models.YahooModel;

import static tamrin.sematec.PublicMethods.weatherCheckType;

public class YahooActivity extends BaseActivity {
    TextView showtemp;
    TextView showtype;
    TextView showdate;
    Button search;
    EditText city;
    String url;
    ImageView weatherimg;
    ImageView showimage;
    ListView yahoolist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo);
        bind();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findByYahoo();
            }
        });

    }


    void bind() {
        search = findViewById(R.id.search);
        showtemp = findViewById(R.id.showtemp);
        showdate = findViewById(R.id.showdate);
        showtype = findViewById(R.id.showtype);
        city = findViewById(R.id.city);
        weatherimg = findViewById(R.id.weatherimg);
        showimage = findViewById(R.id.showimage);
        yahoolist = findViewById(R.id.yahoolist);


    }


    void findByYahoo() {

        url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                city.getText().toString() +
                "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(mContext, "error", Toast.LENGTH_SHORT).show();
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {


                getTemp(responseString);
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void getTemp(String responseString) {

        Gson newgson = new Gson();
        YahooModel weather = newgson.fromJson(responseString, YahooModel.class);


        double showtempc = PublicMethods.converFtoC(Integer.parseInt(weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp()));
        showtemp.setText( showtempc+ ""+ " °C");
        showtype.setText(weather.getQuery().getResults().getChannel().getItem().getCondition().getText());
        showdate.setText(weather.getQuery().getResults().getChannel().getItem().getCondition().getDate());

        String nametype = weather.getQuery().getResults().getChannel().getItem().getCondition().getText();
        weatherCheckType(nametype, showimage);

        ForcastAdapter forcast = new ForcastAdapter(mContext, weather.getQuery().getResults().getChannel().getItem().getForecast());
        yahoolist.setAdapter(forcast);

    }











}

