package tamrin.sematec;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ChampionStageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_champion_stage);

        ImageView winer = findViewById(R.id.winer);
        winer.setImageResource(R.drawable.sunny);
    }
}
