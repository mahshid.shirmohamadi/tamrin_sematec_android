package tamrin.sematec;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.w3c.dom.Text;

public class ForResultActivityB extends BaseActivity {
    EditText sname;
    Button back;
    Button save;
    Button show;
    TextView best1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result_b);
        Hawk.init(mContext).build();
        sname = (EditText) findViewById(R.id.sname);
        back = (Button) findViewById(R.id.back);
        save = (Button) findViewById(R.id.save);
        show = (Button) findViewById(R.id.best);
        best1= (TextView) findViewById(R.id.bestname);



        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String best= Hawk.get("bestname");
               best1.setText(best);

            }
        });






        save.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "thank you", Toast.LENGTH_LONG).show();
                Hawk.put("bestname", sname.getText().toString());
                sname.setText(" ");
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent rintent = new Intent();
                rintent.putExtra("name", sname.getText().toString());
                setResult(Activity.RESULT_OK, rintent);
                finish();

            }
        });

    }

}
