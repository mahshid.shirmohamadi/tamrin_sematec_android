package tamrin.sematec;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by masheloo on 12/20/2017.
 */

public class StudentsModels  {

    String studentname;
    String fathername;
    int studentid;
    String imagurl;


    public StudentsModels() {

    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public String getImagurl() {
        return imagurl;
    }

    public void setImagurl(String imagurl) {
        this.imagurl = imagurl;
    }
}
