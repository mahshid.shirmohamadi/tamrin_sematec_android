package tamrin.sematec;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        bindview();

    }

    void bindview(){
        username= (EditText) findViewById(R.id.name);
        userpass= (EditText) findViewById(R.id.pass);
        userfathername= (EditText) findViewById(R.id.fathername);
        useridnumber= (EditText) findViewById(R.id.idnumber);
        userage= (EditText) findViewById(R.id.age);
        userjob= (EditText) findViewById(R.id.job);
        useraddress= (EditText) findViewById(R.id.address);
        showbutton= (Button)findViewById(R.id.buttonshow);
        result=(TextView) findViewById(R.id.result);
        showbutton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String name = username.getText().toString();
        String password= userpass.getText().toString();
        //result.setText(name + " " + password);
        //String ss= result.toString();
        Toast.makeText(mContext, "wellcome" , Toast.LENGTH_LONG).show();
        username.setText(" ");
        userfathername.setText(" ");
        userjob.setText(" ");
        useraddress.setText(" ");
        userage.setText(" ");
        useridnumber.setText(" ");
        userpass.setText(" ");

  Intent intentactivity = new Intent(mContext, ShowActivity.class);
        intentactivity.putExtra("showname",name+password);
        startActivities(new Intent[]{intentactivity});



    }


}
